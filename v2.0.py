# v 2.0 thread * row
#complejidad n
from threading import Thread
from timeit import default_timer as timer
import numpy as np

class Product(Thread):
    def __init__(self):
        Thread.__init__(self)
    
    def dot(self, x, y, z, j):
        dot = 0
        for k in range(len(y)):
            dot += x[z][k] * y[k][j]
        return dot

    
    def run(self, x, y, result, z):
        for j in range(len(y[0])):
            result[z][j] = self.dot(x,y,z,j)
            

def initMatriz(rows, columns, k):
    m = []
    for i in range(0,rows):
        m.append([k]*columns)
    return m

def productM(x, y, result):
    for z in range(len(x)):
        thread = Product()
        thread.run(x,y,result,z)

def benchmark(nt, x, y, result):
    times = []
    for i in range(nt):
        start_t = timer()
        productM(x,y,result)
        end_t = timer()
        time = end_t - start_t
        times.append(time)
    return times

def media(vector):
    return np.mean(vector)

def standard_deviaton(vector):
    return np.std(vector)


x = initMatriz(100,100,1)
#y = initMatriz(5,5,1)
result = initMatriz(100,100,0)

#--------------------
nt = 100
times = benchmark(nt,x,x,result)
me = media(times) 
std = standard_deviaton(times)
print("Media (milliseconds):",me*1000, "  Standard Deviation (milliseconds):",std*1000)