import zmq
import sys

context = zmq.Context()
socket = context.socket(zmq.PAIR)
port = sys.argv[1]
message = sys.argv[2] 
socket.connect("tcp://localhost:"+port)
socket.send_string(message+" "+port)

