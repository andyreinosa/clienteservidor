
import zmq
import sys

def main():
    if len(sys.argv) != 2:
    	print('Enter operation')
    	exit()
    # Create the socket and the context
    context = zmq.Context()
    s = context.socket(zmq.REQ)
    s.connect("tcp://localhost:5555")

    operation = sys.argv[1]
    if operation == "list":
    	s.send_json({"op":"list"})
    	files = s.recv_json()
    	print(files)
    else:
    	s.send_json({"op":"download", "file":operation})
    	file = s.recv()
    	with open("down-" + operation, "wb") as output:
    		output.write(file)

if __name__ == '__main__':
    main()

import zmq
import sys
import os

def loadFiles(path):
	files = {}
	dataDir = os.fsencode(path)
	for file in os.listdir(dataDir):
		filename = os.fsdecode(file)
		print("Loading {}".format(filename))
		files[filename] = file
	return files


def main():
    if len(sys.argv) != 2:
    	print('Enter directory data')
    	exit()
    # Read available files
    print("Serving files from {}".format(sys.argv[1]))
    files = loadFiles(sys.argv[1])
    print("Load info on {} files.".format(len(files)))

    # Create the socket and the context
    context = zmq.Context()
    s = context.socket(zmq.REP)
    s.bind("tcp://*:5555")

    while True:
    	msg = s.recv_json()
    	if msg["op"] == "list":
    		s.send_json({"files": list(files.keys())})
    	elif msg["op"] == "download":
    		with open(sys.argv[1]+msg["file"],"rb") as input:
    			data = input.read()
    			s.send(data)

if __name__ == '__main__':
    main()
