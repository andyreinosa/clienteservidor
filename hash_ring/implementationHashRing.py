# Andres felipe reinosa
# David Lopez

import bisect
import md5
import zmq
from threading import Thread


#---------------------------------------------------------------------------
class Process(Thread):
    def __init__(self, nodes):
        Thread.__init__(self)

        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PAIR)
        self.nodes = nodes

    def create_server(self):
        for key, value in self.nodes.iteritems():
            self.socket.bind(value)
        print (self.nodes) 

        while True:
            message = self.socket.recv_string()
            print("Server Node, received:   " + message)



class Hash_Ring(object):

    def __init__(self):
        self.repl = 1 #number of nodes for defecf
        self.keys = []
        self.nodes = {}

    
    def generate_key(self, stringKey):
        #given string and return hash value
        return long(md5.md5(stringKey).hexdigest(), 16)
      

    def repl_iterator(self, nodename):
        return(self.generate_key("%s:%s" % (nodename, i))
            for i in xrange(self.repl))

    def add_node(self, nodename, node):
        for h in self.repl_iterator(nodename):
            if h in self.nodes:
                raise ValueError("Node name:  %r it already exists." % nodename)
            self.nodes[h] = node
            bisect.insort(self.keys, h)
    
    def remove_node(self, nodename):
        for h in self.repl_iterator(nodename):
            if not self.nodes.get(h): 
                raise ValueError("Node name:  %r not exists." % nodename)
            del self.nodes[h]
            ind = bisect.bisect_left(self.keys, h)
            del self.keys[ind]

    def return_nodes(self):
        return self.nodes
        
        
            

    
#-------------------------------------------------------------------------
hashRing = Hash_Ring()
hashRing.add_node("usa", 'tcp://*:5555')
hashRing.add_node("canada", 'tcp://*:5575')
hashRing.add_node("mexico", 'tcp://*:5675')
hashRing.add_node("panama", 'tcp://*:5875')
nodes = hashRing.return_nodes()

recv_process = Process(nodes)
recv_process.create_server()
#hashRing.remove_node("matias")
