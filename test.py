import numpy as np
from timeit import default_timer as timer

def initMatriz(rows, columns, k):
    m = []
    for i in range(0,rows):
        m.append([k]*columns)
    return m


start_t = timer()
c = initMatriz(100000,100000,0)
end_t = timer()
time = end_t - start_t
print(time*1000)   


#a = np.empty((100000,100000))
#a.fill(0)   
#c = [[0] * 10000000] * 10000000
#[[element] * numcols] * numrows
#b  = np.zeros((100000,100000))
#data = table[row_from:row_to]