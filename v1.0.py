# v 1.0 nxn threads 
# complejidad n^2

from threading import Thread
from timeit import default_timer as timer
import numpy as np

class Product(Thread):
    def __init__(self):
        Thread.__init__(self)
    

    def run(self, x, y, i, j):
        dot = 0
        for k in range(len(y)):
            dot +=  x[i][k] * y[k][j]
        return dot

def initMatriz(rows, columns, k):
    m = []
    for i in range(0,rows):
        m.append([k]*columns)
    return m

def productM(x, y, result):
    for i in range(len(x)):
        for j in range(len(y[0])):
            thread = Product()
            result[i][j] = thread.run(x,y,i,j)   


def benchmark(nt, x, y, result):
    times = []
    for i in range(nt):
        start_t = timer()
        productM(x,y,result)
        end_t = timer()
        time = end_t - start_t
        times.append(time)
    return times

def media(vector):
    return np.mean(vector)

def standard_deviaton(vector):
    return np.std(vector)


x = initMatriz(5,5,1)
y = initMatriz(5,5,1)
result = initMatriz(5,5,0)

#--------------------
nt = 2000
times = benchmark(nt,x,y,result)
me = media(times) 
std = standard_deviaton(times)
print("Media:",me, "Standard Deviation:",std)



