# v 2.1 constant cores
#complejidad k constant
from threading import Thread
from timeit import default_timer as timer
import numpy as np
from concurrent.futures import *
from scipy.sparse import lil_matrix
import sys, math


def fileToMatriz(filename, nodes):
    fo = open(filename, "r")
    A = lil_matrix((nodes,nodes), dtype='float32')
    for line in fo.readlines():
        l = line.rstrip()
        stringArray = l.split(" ")
        A[int(stringArray[1])-1,int(stringArray[2])-1] = int(stringArray[3])

    fo.close()
    return A

class Product(Thread):
    def __init__(self):
        Thread.__init__(self)


    def diamond(self, x, row, j, nodes):
        base = math.inf
        for k in range(nodes):
            dot1 = x[row,k] + x[k,j]
            if (dot1 == 0):
                if (row == k and row == j and k == j):
                    dot1 = 0
                else:
                    dot1 = math.inf
                
            base = min(dot1,base)
 
        return base

    def run(self, x, result, z, nodes):
        for j in range(nodes):
            result[z,j] = self.diamond(x,z,j,nodes)
            


def productM(x, result, nodes):
    for z in range(nodes):
        thread = Product()
        thread.run(x,result,z,nodes)


def mLog(x, result, nodes,i):
    #executor = ThreadPoolExecutor()
    if (i > math.log(nodes,2)+1):
        return result
    elif (i == 1):
        #executor.submit(productM(x,result,nodes))
        productM(x,result,nodes)
    else:
        #executor.submit(productM(result,result,nodes))
        productM(x,result,nodes)
        mLog(nodes,i+1)


def benchmark(nt, nodes, x, result):
    times = []
    for i in range(nt):
        i = 1
        start_t = timer()
        mLog(x,result,nodes,i)
        end_t = timer()
        time = end_t - start_t
        times.append(time)
    return times

def media(vector):
    return np.mean(vector)

def standard_deviaton(vector):
    return np.std(vector)


nt = 1
nodes = 7
filename = 'test.txt'
x = fileToMatriz(filename, nodes)
print(x.todense())
result = lil_matrix((nodes,nodes), dtype='float32')
times = benchmark(nt,nodes,x,result)
print(result.todense())
me = media(times) 
std = standard_deviaton(times)
print("Media (milliseconds):",me*1000, "  Standard Deviation (milliseconds):",std*1000)
