# v 3.0 thread * row MAX < 8 and diamante
from threading import Thread
from readfile import *
from time import time

class Product(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.t = []
    

    def run(self, a, b, c):
        r = int(a) + int(b)
        #tener en cuenta que el cero siempre esta inicializado en la matriz
        if (r <= c or c == 0):
            return r
        else:
            return c


# tamaño de la matriz nodosXnodos
x1 = readToMatriz('usa.txt', 4, 4) 
x2 = readToMatriz('usa.txt', 4, 4)
result = initMatriz(4,4)

printMatriz(x1)
print("\n")
printMatriz(x2)

start_time = time()
for i in range(len(x1)):
    
    if (i < 8):
        thread = Product()
        print("hilo: ",i)
    for j in range(len(x2[0])):
        for k in range(len(x2)):
            result[i][j] = thread.run(x1[i][k], x2[k][j], int(result[i][j]))

end_time = time()

printMatriz(result)
print("\n time seconds: ", (end_time - start_time))
           
