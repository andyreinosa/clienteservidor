# v 2.1 constant cores
#complejidad k constant
from threading import Thread
from timeit import default_timer as timer
import numpy as np
from concurrent.futures import *

class Product(Thread):
    def __init__(self):
        Thread.__init__(self)
    

    def dot(self, x, y, row, j):
        dot = 0
        for k in range(len(y)):
            dot += x[row][k] * y[k][j]
        return dot

        
    def run(self, x, y, result, z):
        for j in range(len(y[0])):
            result[z][j] = self.dot(x,y,z,j)
            

def initMatriz(rows, columns, k):
    m = []
    for i in range(0,rows):
        m.append([k]*columns)
    return m

def productM(x, y, result):
    for z in range(len(x)):
        thread = Product()
        thread.run(x,y,result,z)

def benchmark(nt, x, y, result):
    executor = ThreadPoolExecutor()
    times = []
    for i in range(nt):
        start_t = timer()
        executor.submit(productM(x,y,result))
        end_t = timer()
        time = end_t - start_t
        times.append(time)
    return times

def media(vector):
    return np.mean(vector)

def standard_deviaton(vector):
    return np.std(vector)


x = initMatriz(100,100,1)
#y = initMatriz(25,25,1)
result = initMatriz(100,100,0)

#--------------------
nt = 100
times = benchmark(nt,x,x,result)
me = media(times) 
std = standard_deviaton(times)
print("Media (milliseconds):",me*1000, "  Standard Deviation (milliseconds):",std*1000)